/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.time

import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import de.ccc.events.badge.card10.LEDS_ABOVE_CHARACTERISTIC_UUID
import de.ccc.events.badge.card10.ROCKETS_CHARACTERISTIC_UUID
import de.ccc.events.badge.card10.TIME_CHARACTERISTIC_UUID
import de.ccc.events.badge.card10.common.ConnectionService
import java.nio.ByteBuffer

class Card10Service(
    service: BluetoothGattService
) {
    private val timeCharacteristic = service.getCharacteristic(TIME_CHARACTERISTIC_UUID)
    private val rocketsCharacteristic = service.getCharacteristic(ROCKETS_CHARACTERISTIC_UUID)
    private var ledsAboveCharacteristic = service.getCharacteristic(LEDS_ABOVE_CHARACTERISTIC_UUID)

    init {
        timeCharacteristic.writeType = BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE
        rocketsCharacteristic.writeType = BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE
        ledsAboveCharacteristic.writeType = BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE
    }

    fun setTime() {
        val buffer = ByteBuffer.allocate(8)
        buffer.putLong(System.currentTimeMillis())
        timeCharacteristic.value = buffer.array()
        ConnectionService.writeCharacteristic(timeCharacteristic)
    }

    fun setRocketValue(value: ByteArray) {
        rocketsCharacteristic.value = value
        ConnectionService.writeCharacteristic(rocketsCharacteristic)
    }

    fun setLeds(value: ByteArray) {
        ledsAboveCharacteristic.value = value
        ConnectionService.writeCharacteristic(ledsAboveCharacteristic)
    }
}