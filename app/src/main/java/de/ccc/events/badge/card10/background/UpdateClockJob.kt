/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.background

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import de.ccc.events.badge.card10.CARD10_BLUETOOTH_MAC_PREFIX
import de.ccc.events.badge.card10.CARD10_SERVICE_UUID
import de.ccc.events.badge.card10.TIME_CHARACTERISTIC_UUID
import java.nio.ByteBuffer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class UpdateClockJob(context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {
    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    override fun doWork(): Result {
        System.out.println("=== doWork() job started")
        for (device in bluetoothAdapter.bondedDevices) {
            if (device.address.startsWith(CARD10_BLUETOOTH_MAC_PREFIX, true)) {
                System.out.println("=== doWork() ${device}")
                var gatt: BluetoothGatt? = null
                try {
                    val gattLatch = CountDownLatch(1);
                    var writeLatch: CountDownLatch? = null
                    var timeCharacteristic: BluetoothGattCharacteristic? = null
                    val callback = object : BluetoothGattCallback() {
                        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
                            System.out.println("=== doWork() onConnectionStateChange " + gatt + " / " + status + " / " + newState)
                            if (newState == BluetoothGatt.STATE_CONNECTED)
                                gatt.discoverServices()
                        }

                        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
                            System.out.println("=== doWork() onServicesDiscovered " + gatt + " / " + status)
                            val card10Service = gatt.getService(CARD10_SERVICE_UUID)
                            timeCharacteristic = card10Service.getCharacteristic(TIME_CHARACTERISTIC_UUID)
                            gattLatch.countDown()
                        }

                        override fun onCharacteristicWrite(
                            gatt: BluetoothGatt?,
                            characteristic: BluetoothGattCharacteristic?,
                            status: Int
                        ) {
                            writeLatch?.countDown()
                        }
                    }
                    gatt = device.connectGatt(applicationContext, true, callback)
                    if (!gattLatch.await(1, TimeUnit.MINUTES)) {
                        System.out.println("=== doWork() connectGatt timed out")
                    } else {
                        val buffer = ByteBuffer.allocate(8)
                        buffer.putLong(System.currentTimeMillis())
                        timeCharacteristic?.value = buffer.array()
                        if (gatt.writeCharacteristic(timeCharacteristic)) {
                            writeLatch = CountDownLatch(1)
                            if (!writeLatch.await(1, TimeUnit.MINUTES))
                                return Result.failure()
                            System.out.println("=== doWork() updated time")
                        }
                    }
                } catch (x: Exception) {
                    x.printStackTrace()
                } finally {
                    gatt?.close();
                }
            }
        }

        System.out.println("=== doWork() job finished")
        return Result.success()
    }
}
