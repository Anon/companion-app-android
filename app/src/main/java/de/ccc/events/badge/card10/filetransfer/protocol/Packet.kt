/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.filetransfer.protocol

import java.nio.ByteBuffer
import java.util.zip.CRC32

data class Packet(val type: PacketType, val payload: ByteArray) {

    companion object {
        fun fromBytes(bytes: ByteArray): Packet {
            val type = when(bytes[0].toChar()) {
                's' -> PacketType.START
                'S' -> PacketType.START_ACK
                'c' -> PacketType.CHUNK
                'C' -> PacketType.CHUNK_ACK
                'f' -> PacketType.FINISH
                'F' -> PacketType.FINISH_ACK
                'e' -> PacketType.ERROR
                'E' -> PacketType.ERROR_ACK
                else -> throw InvalidPacketException()
            }

            val payload = bytes.slice(1 until bytes.size)

            return Packet(type, payload.toByteArray())
        }
    }

    fun getBytes(): ByteArray {
        val buffer = ByteBuffer.allocate(payload.size + 1)
        buffer.put(type.prefix.toByte())
        buffer.put(payload)

        return buffer.array()
    }

    fun getCrc(): Long {
        // RFC1952 CRC32
        val crc = CRC32()
        crc.update(getBytes())
        return crc.value
    }
}