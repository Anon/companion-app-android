/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.filetransfer

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.UiThread
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import de.ccc.events.badge.card10.CARD10_BLUETOOTH_MAC_PREFIX
import de.ccc.events.badge.card10.R
import de.ccc.events.badge.card10.common.ConnectionException
import de.ccc.events.badge.card10.common.ConnectionService
import de.ccc.events.badge.card10.common.GattListener
import de.ccc.events.badge.card10.main.MainFragment
import java.lang.Exception
import java.lang.IllegalStateException

private const val TAG = "FileTransferFragment"
private const val INTENT_RESULT_CODE_FILE = 1

class FileTransferFragment : Fragment(), GattListener, FileTransferListener{
    private var isSending = false
    private var transfer: FileTransfer? = null

    private lateinit var buttonPickFile: Button
    private lateinit var buttonStartStop: Button
    private lateinit var tvSelected: TextView
    private lateinit var tvStatus: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var inputDestination: EditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.file_transfer_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tvSelected = view.findViewById(R.id.label_selected)
        tvStatus = view.findViewById(R.id.label_status)
        progressBar = view.findViewById(R.id.progress)
        inputDestination = view.findViewById(R.id.input_destination)

        buttonPickFile = view.findViewById(R.id.button_pick_file)
        buttonPickFile.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "*/*"
            startActivityForResult(intent, INTENT_RESULT_CODE_FILE)
        }

        buttonStartStop = view.findViewById(R.id.button_start_stop_transfer)

        try {
            toggleControls()
        } catch (e: ConnectionException) {
            showError(e.message)
        } catch (e: Exception) {
            showError(getString(R.string.connection_error_generic))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode != INTENT_RESULT_CODE_FILE) {
            return
        }

        val ctx = context ?: throw IllegalStateException()

        val uri = data?.data ?: return

        try {
            val reader = ChunkedReader(ctx, uri, ConnectionService.mtu)
            val service = ConnectionService.leService ?: throw IllegalStateException()

            transfer = FileTransfer(service, reader, this, inputDestination.text.toString())
        } catch (e: Exception) {
            Log.e(TAG, "Failed to initialize transfer")
            return
        }

        buttonStartStop.isEnabled = true
        tvSelected.text = uri.path
    }

    private fun toggleControls() {
        if (isSending) {
            activity?.runOnUiThread {
                progressBar.visibility = View.VISIBLE
                buttonPickFile.isEnabled = false
                buttonStartStop.text = getString(R.string.file_transfer_button_stop_transfer)
            }

            buttonStartStop.setOnClickListener {
                transfer?.abort()
                transfer = null
                isSending = false

                toggleControls()
            }
        } else {
            activity?.runOnUiThread {
                progressBar.visibility = View.INVISIBLE
                buttonPickFile.isEnabled = true
                buttonStartStop.text = getString(R.string.file_transfer_button_start_transfer)
            }

            buttonStartStop.setOnClickListener {
                transfer?.start()
                isSending = true

                tvStatus.text = "STARTED"
                toggleControls()
            }
        }

        activity?.runOnUiThread {
            buttonStartStop.isEnabled = transfer != null
        }
    }

    override fun onError() {
        activity?.runOnUiThread {
            tvStatus.text = "ERROR"
        }

    }

    override fun onFinish() {
        isSending = false
        transfer = null

        activity?.runOnUiThread {
            tvStatus.text = "FINISHED"
        }
        toggleControls()
    }

    private fun showError(message: String?) {
        val ctx = context ?: throw IllegalStateException()
        val fm = fragmentManager ?: throw IllegalStateException()

        val errorDialog =
            AlertDialog.Builder(ctx)
                .setMessage(message ?: getString(R.string.connection_error_generic))
                .setPositiveButton(R.string.dialog_action_ok) {
                    dialog, _ -> dialog.dismiss()
                    fm.beginTransaction()
                        .replace(R.id.fragment_container, MainFragment())
                        .addToBackStack(null)
                        .commit()
                }
                .show()
    }
}